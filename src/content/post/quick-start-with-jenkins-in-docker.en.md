---
author: "Gustavo Apolinario"
date: 2018-03-08
title: "Quick start with jenkins in docker"
tags: [
    "jenkins",
    "docker",
    "pipeline",
    "blue ocean",
    "devops",
]
categories: [
    "jenkins"
]
---


## Introduction

What’s Jenkins? Jenkins is a automation tool. With it, you can create jobs to build, test and deploy your application.

{{< figure src="/jenkins/jenkins-docker-pipeline.jpg" alt="Jenkins, docker and Pipeline" class="center" >}}

You can create your pipelines in jenkins. But,  whats’s pipeline? Pipeline is a script step by step of all things that you need to do. Example: you will create a pipeline to build your code. First step is checkout your git code, after this you need test, build, package, create a docker image and finaly, deploy your code.

<!--more-->

## Creating Container

The fist steps with jenkins it's create a container in your machine.
You need docker installed and running in your machine.
Execute this command:

```sh
docker run -—name jenkinsci -p 8080:8080 jenkins/jenkins:lts
```

This command will download and run a jenkins in your machine.

Wait a time, The console will show a lot of messages. When jenkins was inicialized, the console will show you a admin password to start jenkins initial setup. The text with password is:

```sh
*************************************************************
*************************************************************
*************************************************************
Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:
a3c76c0cc3e541ff94b3670c22995342
This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
*************************************************************
*************************************************************
*************************************************************
```

## Jenkins instalation Wizard

You can access your jenkins in http://localhost:8080/. You will see this screen:

{{< figure src="/jenkins/installing/jenkins-installing-unlock.png" alt="Jenkins unlock screen" class="center" >}}

Put the admin password in input on the screen and click on "continue".

In next screen, click on “Install suggested plugins”.

{{< figure src="/jenkins/installing/installing-customize.png" alt="Customize jenkins screen" class="center" >}}

Wait the plugins instalations.

{{< figure src="/jenkins/installing/installing-customize-2.png" alt="Getting start screen. Downloading plugins" class="center" >}}

Now, create a user for you.

{{< figure src="/jenkins/installing/installing-create-user.png" alt="Create user screen" class="center" >}}

We finish the instalation, click on “Get start using Jenkins”.

{{< figure src="/jenkins/installing/installing-finished.png" alt="Jenkins is ready" class="center" >}}

## Startup Configuration

Let’s install two plugins:

- blueocean plugin to beautify your jenkins. This plugin better the usability of jenkins and focus in pipelines scripts.
- locale plugin to jenkins always show english text. You don’t need install this, but I suggest install to search errors directly in english.

### Installing locale plugin

Go to Plugin Manager and search for locale.

To install plugins:
In home of jenkins, click on **Manage Jenkins**:

{{< figure src="/jenkins/jenkins-menu-without-blueocean.png" alt="jenkins menu" class="center" >}}

Click on **Manage Plugins**:

{{< figure src="/jenkins/manage/plugins/manage-plugins.png" alt="Manage Plugins link" class="center" >}}

In Plugin manager, you'll need click on **Available** tab and after load the tab, search the plugins.

{{< figure src="/jenkins/manage/plugins/all-tabs.png" alt="Tabs to search and manage plugins" class="center" >}}

Search for **locale**, mark the checkbox and click in install button.

{{< figure src="/jenkins/manage/plugins/check-locale.png" alt="locale plugin" class="center" >}}

### Configuring locale

After install locale, go to Home > Manage Jenkins > Configure System.

{{< figure src="/jenkins/manage/configure-jenkins/configure-jenkins.png" alt="Configure Jenkins" class="center" >}}

Search for locale. Input **“en”** on **“Default language”** and check **“ignore browser preference and force this language to all users”**

{{< figure src="/jenkins/manage/configure-jenkins/configure-jenkins-locale.png" alt="Locale configured" class="center" >}}

### Installing blue ocean plugin

Locale Plugin installed and configured. Let's install Blue ocean.
Back to jenkins home > Manage jenkins > Manage Plugins. On Available tab search for blueocean and install it.

{{< figure src="/jenkins/manage/plugins/check-blue-ocean.png" alt="blueocean plugin" class="center" >}}

### Changing visual to blue ocean

All done. But the visual it's without blue ocean plugin, let’s use it.
On Jenkins home, click on **“Open Blue Ocean”** (left menu).

{{< figure src="/jenkins/jenkins-menu.png" alt="jenkins menu with blue ocean" class="center" >}}

Now, you will see a screen like this:

{{< figure src="/jenkins/jenkins-blueocean-pipeline.png" alt="Blue ocean" class="center" >}}

All Right, the jenkins is installed and the blue ocean plugin is being used to have a better experience in your pipelines.

The next steps is create a pipeline to build some things.
