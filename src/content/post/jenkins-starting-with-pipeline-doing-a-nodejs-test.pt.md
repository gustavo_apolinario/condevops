---
author: "Gustavo Apolinario"
date: 2018-04-15
title: "Iniciando com Pipeline no Jenkins executando teste de Node.js"
slug: "iniciando-com-pipeline-no-jenkins-executando-teste-de-nodejs"
tags: [
    "jenkins",
    "pipeline",
    "devops",
    "node.js",
    "build",
    "test"
]
categories: [
    "jenkins"
]
---

## Intrudução

Para este tutorial, você precisa do jenkins instalado. Este pode rodar em docker com o seguinte tutorial: [Iniciando com jenkins no docker]({{< ref "post/quick-start-with-jenkins-in-docker.pt.md" >}}).

O pipeline é melhor pois:

- você pode reutilizar tudo que já fez
- você pode colocar seu código de build dentro do projeto, junto do seu código
- você pode versionar o código do job
- as mudanças feitas no pipeline são mostradas em “changes” dentro do “job history”

Todas mudanças no git iniciarâo o job. Com Jenkinsfile (arquivo com o código do pipeline) dentro do seu projeto git, todas as mudanças do pipeline ou do seu projeto iniciarão o job e as modificações ficarão no log.

<!--more-->

## Instalando a ferramenta Node.js

Na home do jenkins, clique em **Manage Jenkins**:

{{< figure src="/jenkins/jenkins-menu.png" alt="menu do jenkins" class="center" >}}

Clique em **Manage Plugins**:

{{< figure src="/jenkins/manage/plugins/manage-plugins.png" alt="Manage Plugins" class="center" >}}

Em Plugin manager, clique na aba **Available**  e depois de carregá-la, busque o plugin.

{{< figure src="/jenkins/manage/plugins/all-tabs.png" alt="Aba para buscar e gerenciar plugins" class="center" >}}

Busque por **nodejs** e marque seu checkbox.

{{< figure src="/jenkins/manage/plugins/check-nodejs.png" alt="Nodejs plugin" class="center" >}}

Clique no botão “Download now and install after restart”.

{{< figure src="/jenkins/manage/plugins/button-download-and-restart.png" alt="Botão Download now and install after restart" class="center" >}}

Na próxima tela, marque o checkbox “Restart Jenkins when installation is complete and no jobs are running”.

{{< figure src="/jenkins/manage/plugins/installing-nodejs-restart.png" alt="Instalando Plugins com reinicialização marcada" class="center" >}}

Aguarde um tempo, se a tela não mudar, vá para a home do jenkins clicando no logo do jenkins que fica no topo esquerdo do site.

## Configurando a ferramenta Node.js

Depois de instalar e reiniciar, vá para a home do jenkins > Manage Jenkins > Global Tool Configuration.

{{< figure src="/jenkins/manage/global-config/global-config.png" alt="Global Tool Configuration" class="center" >}}

Busque por NodeJS e clique no botão NodeJS instalation.

{{< figure src="/jenkins/manage/global-config/node-installation.png" alt="Botão NodeJS Installations" class="center" >}}

Coloque um nome para a configuração do node, ex: **node**.
Selecione a versão do node que você precisa.

{{< figure src="/jenkins/manage/global-config/nodejs-configuration.png" alt="configurações do NodeJS" class="center" >}}

## Teste o plugin nodejs

OK. NodeJS instalado com sucesso. Vamos testa-lo.

Vá para a home do jenkins e clique em **New Item** no menu do jenkins.

{{< figure src="/jenkins/jenkins-menu.png" alt="menu do jenkins" class="center" >}}

Vamos criar um job. Nomeie-o com **build-test**  e selecione pipeline. Clique em "OK".

{{< figure src="/jenkins/jobs/node-js/build-test/new-build-test.png" alt="criando job" class="center" >}}

Você verá isso:

{{< figure src="/jenkins/jobs/node-js/build-test/new-build-test-1.png" alt="Abas do job" class="center" >}}

No pipeline, cole o código

```jenkinsfile
pipeline {
  agent any
 
  tools {nodejs “node”}
 
  stages {
    stage(‘Example’) {
      steps {
        sh ‘npm config ls’
      }
    }
  }
}
```

O job ficará assim:

{{< figure src="/jenkins/jobs/node-js/build-test/new-build-test-pipeline.png" alt="Pipeline Script" class="center" >}}

### Explicação do Pipeline

Antes de continuar, vamos entender o pipeline:

```jenkinsfile
agent any
```

O job irá executar em qualquer agente do jenkins.

- Podemos ter vários nós do jenkins, um mestre e alguns servos, e o job poderá executar em qualquer um.
- Você pode executar o job em um jenkins específico, ex: Você tem 3 jenkins com Linux e um com Windows. Você pode forçar executar este job somente no jenkins com Windows.

```jenkinsfile
tools {nodejs “node”}
```

Esta linha busca pela ferramenta nodejs, nomeada como “node” e a usa no pipeline. É necessário para os próximos scripts encontrarem o comando node.

```jenkinsfile
stages {
```

Stages são as etapas do pipeline.

```jenkinsfile
stage(‘Example’) {
  steps {
    ...
  }
}
```

Este comando inicia uma nova etapa chamada "Example" e executa os passos dentro da etapa.

```jenkinsfile
sh ‘npm config ls’
```

Executa um script sh na máquina. Com isso, podemos testar se o npm está rodando corretamente.

### Rodando o test

Agora, vamos executar o script. Salve o job. Dentro deste, no menu a esquerda, clique em **Build Now**.

{{< figure src="/jenkins/jobs/job-menu.png" alt="menu do Job" class="center" >}}

Aguarde um tempo para o build aparecer no **build history**  abaixo do menu (Se necessário, recarregue a página).

{{< figure src="/jenkins/jobs/node-js/build-test/build_history.png" alt="Build Histoy" class="center" >}}

Depois do build terminar, o círculo poderá ser vermelho ou azul. Se o círculo for azul, o job foi executado com sucesso.

No centro da página, você pode visualizar as etapas.

{{< figure src="/jenkins/jobs/node-js/build-test/stage-view.png" alt="Stage View" class="center" >}}

Pode se ver o log do build clicando no circulo azul dentro do build history. Você verá isto:

```markdown
Started by user Gustavo
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/build-test
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Declarative: Tool Install)
[Pipeline] tool
[Pipeline] envVarsForTool
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Example)
[Pipeline] tool
[Pipeline] envVarsForTool
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
[build-test] Running shell script
+ npm config ls
; cli configs
metrics-registry = "https://registry.npmjs.org/"
scope = ""
user-agent = "npm/5.6.0 node/v9.10.1 linux x64"

; node bin location = /var/jenkins_home/tools/jenkins.plugins.nodejs.tools.NodeJSInstallation/node/bin/node
; cwd = /var/jenkins_home/workspace/build-test
; HOME = /var/jenkins_home
; "npm config ls -l" to show all defaults.

[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```

Ok. O build de test está certo. Vamos criar um real job com node.


## Testando a aplicação

Vá para a home do jenkins, clique em "new item". Selecione pipeline e crie um job nomeado **“node_test”** e coloque este script:

```jenkinsfile
pipeline {
  agent any
    
  stages {    
    stage('Cloning Git') {
      steps {
        git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'
      }
    }
  }     
}
```

Teste o job, ele irá fazer download de um projeto node para seu jenkins. O stage view precisa ser:

{{< figure src="/jenkins/jobs/node-js/build-final/1-git-download.png" alt="Stage View do git clone" class="center" >}}

Agora, na tela do job. Clique em "configure" e vamos mudar o código.

```jenkinsfile
pipeline {
  agent any
    
  tools {nodejs "node"}
    
  stages {
        
    stage('Cloning Git') {
      steps {
        git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'
      }
    }
        
    stage('Install dependencies') {
      steps {
        sh 'npm install'
        sh 'npm run bowerInstall'
      }
    }
     
    stage('Test') {
      steps {
         sh 'npm test'
      }
    }      
  }
}
```

Depois do passo de clonar, instalaremos as dependências do npm e as dependências do bower usando npm run. Finalmente o teste é executado.

{{< figure src="/jenkins/jobs/node-js/build-final/2-install-dependencies-and-test.png" alt="Stage View com git, download das dependências e npm test" class="center" >}}

A aplicação foi testada com sucesso.

## Pipeline dentro do projeto

Agora, vamos colocar o script do pipeline dentro do projeto git em um arquivo chamado Jenkinsfile.

{{< figure src="/jenkins/jobs/node-js/from-git/jenkinsfile.png" alt="Jenkinsfile dentro do projeto git" class="center" >}}

Volte ao jenkins e mude o job.

Primeira etapa, troque a definição do job pipeline para **“Pipeline script from SCM”**.

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git.png" alt="Pipeline script from SCM" class="center" >}}

Selecione **Git** em SCM Select. Coloque seu repositório git **Repository URL**.

{{< figure src="/jenkins/jobs/node-js/from-git/git-without-credential.png" alt="Build sua pipeline de um repositório git" class="center" >}}

### Credenciais Git

Se você precisar de credenciais para acessar seu repositório git, Clique no botão "Add".

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git-configuration.png" alt="Botão Add credenciais git" class="center" >}}

Adicione suas credenciais git

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git-configuration-credentials.png" alt="Adicione suas credenciais git" class="center" >}}

E a configuração do job está com sua credencial.

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git-configuration2.png" alt="Configuração git com credenciais" class="center" >}}

Temos um projeto node testado. Agora vamos gerar um container docker com nosso projeto.

Em breve
