---
title: "Sobre"
date: 2018-04-14T17:00:05+08:00
lastmod: 2018-04-14T17:00:05+08:00
menu: "main"
weight: 50

# you can close something for this content if you open it in config.toml.
comment: false
mathjax: false
---

ConDevOps - Conexão DevOps é um blog sobre tudo que envolve automação.
Neste blog você verá ferramentas e framworks de automação, como jenkins por exemplo.
Além de informações diretamente ligadas as automações, também teremos artigos que envolvem indiretamente. Um bom exemplo é o uso de microserviços no desenvolvimento. É uma prática de programação, mas facilita bastante para a infraestrutura auto-escalável.

Sem mais delongas, o time que está postando para este blog:


* [Gustavo Apolinario](https://github.com/gustavoapolinario)


