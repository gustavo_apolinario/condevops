---
author: "Gustavo Apolinario"
title: "Jenkins and Slack Integration"
date: 2018-05-01T19:37:21-03:00
tags: [
    "jenkins",
    "pipeline",
    "devops",
    "slack"
]
categories: [
    "jenkins"
]
---

# Introduction

Slack is a chat software. Slack has hook's to provide communication from your tools to your team.

Jenkins can send notification from all your jobs to your team. It can use channels to send especific notification for specific teams.

<!--more-->

# Create Slack Account

First, create your slack account if you don't have yet.

Access [Slack.com](https://slack.com/).

# Instaling Jenkins app inside slack

To Jenkins access slack, you need install the jenkins app.

Go to your slack and click on "+" (see image below)

{{< figure src="/slack/jenkins/installation/slack-browse-app.png" alt="Slack Browse app Button plus" class="center" >}}

Seach for "Jenkins" and click on "install" button

{{< figure src="/slack/jenkins/installation/slack-install-jenkins.png" alt="Slack Install Jenkins button" class="center" >}}

You will be send to Jenkins app page. Click "Install" again

{{< figure src="/slack/jenkins/installation/slack-install-jenkins2.png" alt="Slack other Install Jenkins button" class="center" >}}

Select the Channel for Jenkins post message or create a new channel.

For this tutorial, create a channel named "jenkins".

{{< figure src="/slack/jenkins/installation/slack-install-jenkins3b.png" alt="Slack other Install Jenkins button" class="center" >}}

Right, Slack is configured and show you a tutorial of how to configure jenkins.

Follow the Slack tutorial.

# Installing and configuring slack on jenkins

This step was done in slack tutorial, but let's review.

## Install "Slack Notification" plugin in jenkins

Go to Manage Jenkins > Available tab search for "Slack Notification". Install it and restart jenkins.

## Configuring Slack plugin

Go to Manage Jenkins > Configure System. Find "Global Slack Notifier Settings" in this page and populate with your slack information.

{{< figure src="/jenkins/manage/configure-jenkins/slack-plugin.png" alt="Configure Slack plugin in jenkins" class="center" >}}

After configured click on "Test Connection" button and a new message will be received in slack.

{{< figure src="/slack/jenkins/notifications/slack-jenkins-test.png" alt="Configure Slack plugin in jenkins" class="center" >}}

# Slack test message

Create a new job in jenkins named "slack-test" and put this pipeline script:

```Jenkinsfile
pipeline {
    agent any
    
    stages {
        stage('Slack Message') {
            steps {
                slackSend channel: '#jenkins',
                    color: 'good',
                    message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER}\n More info at: ${env.BUILD_URL}"
            
            }
        }
    }
}
```

This script will send to channel 'jenkins' a message with color named 'good' (green). This message will contain the Job result (SUCCESS/FAILURE), the name of the job, the build number and a link to see the build information.

Run the job and you will receive a new message in slack.

{{< figure src="/slack/jenkins/notifications/build-test.png" alt="Configure Slack plugin in jenkins" class="center" >}}

## Add User in message

To add the user who run the build inside the message, you need create a function with the pipeline.

```Jenkinsfile
def getBuildUser() {
    return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
}
```

Create a new environment for pipeline

```Jenkinsfile
environment {
    BUILD_USER = ''
}
```

Set the environment with the function return.

```Jenkinsfile
script {
    BUILD_USER = getBuildUser()
}
```

Change the message to contain the new information.

The new pipeline is.

```Jenkinsfile
def getBuildUser() {
    return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
}

pipeline {
    environment {
        BUILD_USER = ''
    }
    agent any
    
    stages {
        stage('Slack Message') {
            steps {
                script {
                    BUILD_USER = getBuildUser()
                }
                slackSend channel: '#jenkins',
                    color: 'good',
                    message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${BUILD_USER}\n More info at: ${env.BUILD_URL}"
            
            }
        }
    }
}
```

Before run, you need remove the checkbox "Use Groovy Sandbox" below pipeline.

{{< figure src="/jenkins/jobs/slack/pipeline.png" alt="Configure Slack plugin in jenkins" class="center" >}}

Run and see the new message, now with user information.

{{< figure src="/slack/jenkins/notifications/build-test-with-user.png" alt="Configure Slack plugin in jenkins" class="center" >}}

# Dynamic information about the build

Let's create a new pipeline. This pipeline will send the notification with colors based on status.

First, create a array of colors based on jenkins job status.

```Jenkinsfile
def COLOR_MAP = ['SUCCESS': 'good', 'FAILURE': 'danger', 'UNSTABLE': 'danger', 'ABORTED': 'danger']
```

Now, the color parameter is:

```Jenkinsfile
color: COLOR_MAP[currentBuild.currentResult]
```

To run the slack notification in job failure or success, use post build.

You can learn more about post build on [How to do Post Build in Jenkins Pipeline]({{< ref "post/how-to-do-post-build-in-jenkinsp-ipeline.en.md" >}})


The entire new pipeline:

```Jenkinsfile
def COLOR_MAP = ['SUCCESS': 'good', 'FAILURE': 'danger', 'UNSTABLE': 'danger', 'ABORTED': 'danger']

def getBuildUser() {
    return currentBuild.rawBuild.getCause(Cause.UserIdCause).getUserId()
}

pipeline {
    environment {
        //This variable need be tested as string
        doError = '1'
        BUILD_USER = ''
    }
   
    agent any
    
    stages {
        stage('Error') {
            when {
                expression { doError == '1' }
            }
            steps {
                echo "Failure"
                error "failure test. It's work"
            }
        }
        
        stage('Success') {
            when {
                expression { doError == '0' }
            }
            steps {
                echo "ok"
            }
        }
    }
	post {
        always {
            script {
                BUILD_USER = getBuildUser()
            }
            echo 'I will always say Hello again!'
            
            slackSend channel: '#jenkins',
                color: COLOR_MAP[currentBuild.currentResult],
                message: "*${currentBuild.currentResult}:* Job ${env.JOB_NAME} build ${env.BUILD_NUMBER} by ${BUILD_USER}\n More info at: ${env.BUILD_URL}"
            
        }
    }
}
```

Test your job changing the environment "doError" from '1' to '0'.

The new slack message:

{{< figure src="/slack/jenkins/notifications/slack-jenkins-notification.png" alt="Configure Slack plugin in jenkins" class="center" >}}

Thanks.