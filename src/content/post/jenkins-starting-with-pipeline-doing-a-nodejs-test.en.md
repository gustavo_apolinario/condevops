---
author: "Gustavo Apolinario"
date: 2018-04-15
title: "Jenkins Starting with Pipeline doing a Node.js test"
tags: [
    "jenkins",
    "pipeline",
    "devops",
    "node.js",
    "build",
    "test"
]
categories: [
    "jenkins"
]
---

## Introduction

For this tutorial, you need jenkins installed. You can run in docker with this tutorial [Quick start with jenkins in docker]({{< ref "post/quick-start-with-jenkins-in-docker.en.md" >}}).

The pipeline is better because:

- you can reuse everything you did
- you can put your build code inside project together with your code
- you can version the job code
- the change in pipeline is showed in “changes” inside job history

All changes in git the job is started. With Jenkinsfile(file with pipeline script) inside your git project, all changes in pipeline or your project the job will start and the modification is logged by jenkins.

<!--more-->

## Installing Node.js tool

In home of jenkins, click on **Manage Jenkins**:

{{< figure src="/jenkins/jenkins-menu.png" alt="jenkins menu" class="center" >}}

Click on **Manage Plugins**:

{{< figure src="/jenkins/manage/plugins/manage-plugins.png" alt="Manage Plugins link" class="center" >}}

In Plugin manager, click on **Available** tab and after load the tab, search the plugins.

{{< figure src="/jenkins/manage/plugins/all-tabs.png" alt="Tabs to search and manage plugins" class="center" >}}

Search for **nodejs** and mark the checkbox.

{{< figure src="/jenkins/manage/plugins/check-nodejs.png" alt="Nodejs plugin" class="center" >}}

Click on button “Download now and install after restart”.

{{< figure src="/jenkins/manage/plugins/button-download-and-restart.png" alt="Download now and install after restart button" class="center" >}}

On next screen, mark the checkbox “Restart Jenkins when installation is complete and no jobs are running”.

{{< figure src="/jenkins/manage/plugins/installing-nodejs-restart.png" alt="Installating Plugins with Restart checked" class="center" >}}

Wait a moment, if the screen not change, go to jenkins home clicking in jenkins's logo on top, in the left side of the site.

## Configuring Node.js tool

After installed and restarted, go to jenkins's home > Manage Jenkins > Global Tool Configuration.

{{< figure src="/jenkins/manage/global-config/global-config.png" alt="Global Tool Configuration" class="center" >}}

Search for NodeJS and click on NodeJS instalation button.

{{< figure src="/jenkins/manage/global-config/node-installation.png" alt="NodeJS Installations button" class="center" >}}

Put a name for node configuration, ex: **node**.
Select the version of node that you need.

{{< figure src="/jenkins/manage/global-config/nodejs-configuration.png" alt="NodeJS config" class="center" >}}

## Test the nodejs plugin

OK. NodeJS is installed with sucess. Let’s try it.

Go to jenkins's homne, and click on **New Item** on jenkins's menu.

{{< figure src="/jenkins/jenkins-menu.png" alt="jenkins menu" class="center" >}}

Let’s create a job. Name it with **build-test** and select Pipeline. Click on "OK".

{{< figure src="/jenkins/jobs/node-js/build-test/new-build-test.png" alt="creating job" class="center" >}}

You will see this:

{{< figure src="/jenkins/jobs/node-js/build-test/new-build-test-1.png" alt="job tabs" class="center" >}}

In pipeline, past this code:

```jenkinsfile
pipeline {
  agent any
 
  tools {nodejs “node”}
 
  stages {
    stage(‘Example’) {
      steps {
        sh ‘npm config ls’
      }
    }
  }
}
```

The job will be like this:

{{< figure src="/jenkins/jobs/node-js/build-test/new-build-test-pipeline.png" alt="Pipeline Script" class="center" >}}

### Pipeline explanation

Before continue, let’s understand the pipeline job:

```jenkinsfile
agent any
```

The job will run in any jenkins agent.

- You can have a lot of jenkins nodes, one master and some slaves, and the job will run in any node.
- You can run the job on specific jenkins, ex: You have 3 jenkins with Linux and one with Windows. You can run this job only in jenkins slave with Windows.

```jenkinsfile
tools {nodejs “node”}
```

This line search for nodejs tool named “node” and use it in pipeline. It’s necessary for next scripts find the commands.

```jenkinsfile
stages {
```

Stages is the inicialization from pipeline steps.

```jenkinsfile
stage(‘Example’) {
  steps {
    ...
  }
}
```

This commands start a new stage named “Example” and run the steps inside this stage.

```jenkinsfile
sh ‘npm config ls’
```

It execute a sh script in machine. With this, we can test the npm running correctly.

### Running the test

Now, let’s run the script. Save the job. Inside the job, in left menu, click on **Build Now**.

{{< figure src="/jenkins/jobs/job-menu.png" alt="Job menu" class="center" >}}

Wait some time for the build apear in **build history** below the menu (If necessary, reload the page).

{{< figure src="/jenkins/jobs/node-js/build-test/build_history.png" alt="Build Histoy" class="center" >}}

After the build over, the ball can be red or blue. If the ball is blue, the job is builded with success.

In center of page, you can visualize the stage view.

{{< figure src="/jenkins/jobs/node-js/build-test/stage-view.png" alt="Stage View" class="center" >}}

You can see the build log clicking on blue ball inside build history. You will see this:

```markdown
Started by user Gustavo
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/build-test
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Declarative: Tool Install)
[Pipeline] tool
[Pipeline] envVarsForTool
[Pipeline] }
[Pipeline] // stage
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Example)
[Pipeline] tool
[Pipeline] envVarsForTool
[Pipeline] withEnv
[Pipeline] {
[Pipeline] sh
[build-test] Running shell script
+ npm config ls
; cli configs
metrics-registry = "https://registry.npmjs.org/"
scope = ""
user-agent = "npm/5.6.0 node/v9.10.1 linux x64"

; node bin location = /var/jenkins_home/tools/jenkins.plugins.nodejs.tools.NodeJSInstallation/node/bin/node
; cwd = /var/jenkins_home/workspace/build-test
; HOME = /var/jenkins_home
; "npm config ls -l" to show all defaults.

[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
Finished: SUCCESS
```

Ok. The build test right. Let’s create a real node job.


## Testing the application

Go to jenkins's home, click on "new job". Select pipeline and create the job named **“node_test”**, and put this script:

```jenkinsfile
pipeline {
  agent any
    
  stages {    
    stage('Cloning Git') {
      steps {
        git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'
      }
    }
  }     
}
```

Test this job, it will download a node project to your jenkins. The stage view need be:


{{< figure src="/jenkins/jobs/node-js/build-final/1-git-download.png" alt="Stage View Git clone" class="center" >}}


Now, in job screen. Click on "configure" and let’s change the code.

```jenkinsfile
pipeline {
  agent any
    
  tools {nodejs "node"}
    
  stages {
        
    stage('Cloning Git') {
      steps {
        git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'
      }
    }
        
    stage('Install dependencies') {
      steps {
        sh 'npm install'
        sh 'npm run bowerInstall'
      }
    }
     
    stage('Test') {
      steps {
         sh 'npm test'
      }
    }      
  }
}
```

After cloning step, we install the npm dependencies and the bower dependencies using npm run. Finally, the test is executed.

{{< figure src="/jenkins/jobs/node-js/build-final/2-install-dependencies-and-test.png" alt="Stage View with git, download dependencies and npm test" class="center" >}}

All right, the application is tested with success.

## Pipeline inside project

Now, put the script of pipeline inside your git project in a file named Jenkinsfile.

{{< figure src="/jenkins/jobs/node-js/from-git/jenkinsfile.png" alt="Jenkinsfile inside git project" class="center" >}}

Go back to jenkins and change the job.

First step, change the definition of pipeline job to **“Pipeline script from SCM”**.

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git.png" alt="Pipeline script from SCM" class="center" >}}

Select **Git** on SCM Select. Put your git repository on **Repository URL**.

{{< figure src="/jenkins/jobs/node-js/from-git/git-without-credential.png" alt="Build your pipeline from git repository" class="center" >}}

### Git credentials

If you need credential to access your git repository, click on "Add" button.

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git-configuration.png" alt="Button Add git Credentials" class="center" >}}

Add your git credentials.

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git-configuration-credentials.png" alt="Add Credentials to git" class="center" >}}

And the configuration will be right with your credentials.

{{< figure src="/jenkins/jobs/node-js/from-git/pipeline-from-git-configuration2.png" alt="Git configuration with credentials" class="center" >}}

We have the node project tested. Now we can build the docker container with our project.

Soon.
