---
title: "About"
date: 2018-04-14T17:00:05+08:00
lastmod: 2018-04-14T17:00:05+08:00
menu: "main"
weight: 50

# you can close something for this content if you open it in config.toml.
comment: false
mathjax: false
---

ConDevOps - Connextion DevOps is a blog about everything around automation.
In this blog, you will see the automation tools and frameworks, like Jenkins.
besides information directly about automation, we will also have posts that indirectly involve that. A good example is microservices. It's a development technique, but it makes easier for self-scalable infrastructure.

Without further ado, the team that is posting for this blog:


* [Gustavo Apolinario](https://github.com/gustavoapolinario)
