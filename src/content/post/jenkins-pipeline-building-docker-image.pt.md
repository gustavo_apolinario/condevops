---
author: "Gustavo Apolinario"
title: "Jenkins Pipeline Building Docker Image"
date: 2018-04-19T20:51:57-03:00
draft: true
tags: [
    "jenkins",
    "pipeline",
    "devops",
    "docker",
    "build",
    "container",
    "image"
]
categories: [
    "jenkins"
]
---


## Introdução

Olá, neste tutorial nós iremos criar uma imagem docker com jenkins.

O que é docker?
Docker é uma plataforma aberta para desenvolvedores e sysadmins fazerem build, enviarem e executarem seus aplicativos distribuidos.

Você pode ler mais sobre docker em [docker website](https://www.docker.com/)

Porque pipeline?
você pode reusar tudo que fez, colocar seu codigo jenkins dentro do projeto no git, as mudanças no pipeline são mostradas em "changes" dentro do job history.

O que é dockerhub?
Dockerhub é um docker registry publico para armazenas suas imagens docker. Se você precisa de um registry privativo, você pode pagar por ele. Nós iremos usa-lo pois é o docker registry mais fácil.

<!--more-->

## Jenkins com docker instalado

Nós iremos usar alguns códigos pipelines que precisam do docker instalado no jenkins para encontrar os comandos.

Eu criei uma imagem docker do jenkins com o docker instalado.

Vamos olhar o Dockerfile:

```Dockerfile
FROM jenkins/jenkins:lts

USER root

RUN apt-get update && \
apt-get -y install apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common && \
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg > /tmp/dkey; apt-key add /tmp/dkey && \
add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") \
   $(lsb_release -cs) \
   stable" && \
apt-get update && \
apt-get -y install docker-ce

RUN apt-get install -y docker-ce

RUN usermod -a -G docker jenkins

USER jenkins
```

Este Dockerfile é gerado a partir da imagem oficial do jenkins, instala o docker e da acesso ao usuário jenkins para dar biuld nos dockers.


Você pode dar build nesta imagem, mas ela já se encontra no dockerhub [gustavoapolinario/jenkins-docker](https://hub.docker.com/r/gustavoapolinario/jenkins-docker/).

## Rodando jenkins com docker do host

Para rodar o container, você precisa adicionar um volume no comando docker run.

```sh
... -v /var/run/docker.sock:/var/run/docker.sock ...
```
Isto irá compartilhar o socket docker (usado no seu máquina) com o container.

O comando run completo:

```sh
docker run --name jenkins-docker -p 8080:8080 -v /var/run/docker.sock:/var/run/docker.sock gustavoapolinario/jenkins-docker
```

Depois de inicializar o jenkins, compete a configuração inicial.

Leia este tutorial para completar a configuração e instalar os plugins locale e blueocean: [Iniciando com jenkins no docker]({{< ref "post/quick-start-with-jenkins-in-docker.pt.md" >}}).

## Criando um job para testar o comando docker


Na home do jenkins, clique em "New Item", selecione "Pipeline" e coloque o nome do job como "docker-test".

{{< figure src="/jenkins/jobs/docker/build-test/new-job.png" alt="New pipeline Job" class="center" >}}

Coloque este script dentro do job:

```Jenkinsfile
pipeline {
    environment {
        registry = "docker_hub_account/repository_name"
        registryCredential = 'dockerhub'
    }
   
    agent any
    
    stages {
    	
    	stage('Building image') {
            steps{
                script {
                    docker.build registry + ":$BUILD_NUMBER"
                }
    	    }
    	}
    }
}
```

A tela ficará como esta:

{{< figure src="/jenkins/jobs/docker/build-test/pipeline.png" alt="Pipeline in job config" class="center" >}}

Salve o job.

### Explicando o Pipeline

Neste pipeline, nós temos 2 variáveis de ambiente para mudar o registry e a credencial facilmente.

```Jenkinsfile
environment {
    registry = "docker_hub_account/repository_name"
    registryCredential = 'dockerhub'
}
```

O job irá ter um passo. Ele irá executar o build do docker e usará o número de build do jenkins na tag do docker.
Com o número de build facilita para dar deploy e rollback baseado no jenkins. Claro que você pode colcoar a tag que você precisa.

```Jenkinsfile
    stages {
    	stage('Building image') {
            steps{
                script {
                    docker.build registry + ":$BUILD_NUMBER"
                }
    	    }
    	}
    }
}
```

### Testando o comando docker no job

Clique em "Build Now" no menu do job.

{{< figure src="/jenkins/jobs/job-menu.png" alt="Menu do Job" class="center" >}}

O job irá falhar. Não se preocupe.

{{< figure src="/jenkins/jobs/docker/build-test/build-failure.png" alt="Falha no Build" class="center" >}}

Na home do job, você pode clicar no circulo e ver a saída do console.

{{< figure src="/jenkins/jobs/docker/build-test/build-history.png" alt="Histórico de Build" class="center" >}}

Ou click no número do buil (**#1**) e clique em "Console Output".

{{< figure src="/jenkins/jobs/job-build-menu.png" alt="Menu do Job" class="center" >}}

Na saída do console você verá isto:

```JenkinsLog
Started by user GUSTAVO WILLY APOLINARIO DOMINGUES
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] node
Running on Jenkins in /var/jenkins_home/workspace/docker-test
[Pipeline] {
[Pipeline] withEnv
[Pipeline] {
[Pipeline] stage
[Pipeline] { (Building image)
[Pipeline] script
[Pipeline] {
[Pipeline] sh
[teste234] Running shell script
+ docker build -t docker_hub_account/repository_name:1 .
unable to prepare context: unable to evaluate symlinks in Dockerfile path: lstat /var/jenkins_home/workspace/docker-test/Dockerfile: no such file or directory
[Pipeline] }
[Pipeline] // script
[Pipeline] }
[Pipeline] // stage
[Pipeline] }
[Pipeline] // withEnv
[Pipeline] }
[Pipeline] // node
[Pipeline] End of Pipeline
ERROR: script returned exit code 1
Finished: FAILURE
```

O erro acontece pois o Dockerfile não foi encontrado. Nós iremos resolver isso em breve.

O comando docker foi executado e o jenkins controu o comando.

## criando um repositório no dockerhub

Crie uma conta no dockerhub, se ainda não tiver.

[dockerhub](https://hub.docker.com/)

Logado no dockerhub, clique em "Create" > "Create Repository".

{{< figure src="/dockerhub/docker-hub-create.png" alt="Menu do Dockerhub para criar o Repositório" class="center" >}}

Coloque um nome para seu repositório. Para este exemplo, use "docker-test".

{{< figure src="/dockerhub/creating-repository.png" alt="Criando repositório Dockerhub" class="center" >}}

Depois de criar o repositório docker, pegue seu nome para usar no nosso pipeline. Em meu caso, o nome é **"gustavoapolinario/docker-test"**.

{{< figure src="/dockerhub/repository-screen.png" alt="Repositório Dockerhub" class="center" >}}

## Criando credencial dockerhub

Vá para a home do jenkins, clique  em "credentials" e em "**(global)**".

{{< figure src="/jenkins/credentials/credentials-screen.png" alt="Credenciais" class="center" >}}

Clique em "Add Credentials" no menu da esquerda.

{{< figure src="/jenkins/credentials/global-credentials-menu.png" alt="Credenciais globais" class="center" >}}

Coloque sua credencial e salve..

{{< figure src="/jenkins/credentials/dockerhub-credential.png" alt="Criando Credenciais Dockerhub" class="center" >}}

Lembre-se de mudar a variável de credencial (registryCredential) se você não colocar **"dockerhub"** no ID da credencial criada.

A credencial está configurada.

## Configurando a variável do dockerhub

Altere a pipeline do job. Vá na home do jenkins, clique no nome do job (docker-test), clique em "configure" no menu do job.

O código que você precisa mudar é:

```Jenkinsfile
environment {
    registry = "docker_hub_account/repository_name"
    registryCredential = 'dockerhub'
}
```

Mude a variável de ambiente "registry" para seu nome de repositório. Em meu caso é **"gustavoapolinario/docker-test"**.

Mude a variável de amebiente "registryCredential" se necessário.

Minhas variáveis ficaram:

```Jenkinsfile
environment {
    registry = "gustavoapolinario/docker-test"
    registryCredential = 'dockerhub'
}
```

## Criando a primeira imagem docker

Com a credencial do dockerhube o repositório criado, o jenkins pode enviar a imagem docker gerada para o dockerhub (nosso repositório docker).

Neste exemplo, vamos dar build em uma aplicação node.js.

Nós precisamos de um Dockerfile para dar o build.

Vamos criar um novo passo no pipeline para dar clone de um repositório git que possui um Dockerfile dentro.

```Jenkinsfile
stage('Cloning Git') {
    steps {
        git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'
    }
}
```

O pipeline deve ficar (mas usando suas environment):

```Jenkinsfile
pipeline {
    environment {
        registry = "gustavoapolinario/docker-test"
        registryCredential = 'dockerhub'
    }
   
    agent any
    
    stages {
        stage('Cloning Git') {
            steps {
                git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'
            }
        }
        
    	stage('Building image') {
            steps{
                script {
                    docker.build registry + ":$BUILD_NUMBER"
                }
    	    }
    	}
	}
}
```

Salve e execute-o clicando em "Build now".

O "Stage View " no job do jenkins irá mudar para isso:

{{< figure src="/jenkins/jobs/docker/build-test/git-docker-build.png" alt="Stage View com git" class="center" >}}

## Enviando a imagem docker para o dockerhub

Até este momento, nós clonamos o git e geramos a imagem do docker.

Precisamos colocar a imagem em um repositório docker para pega-la em outras máquinas.

Primeiro, crie uma variável para salvar as informações da imagem do docker.

```Jenkinsfile
dockerImage = ''
```

Mude o build para salvar na variável.

```Jenkinsfile
dockerImage = docker.build registry + ":$BUILD_NUMBER"
```

Crie um novo passo para enviar a imagem docker gerada para o dockerhub.

```Jenkinsfile
stage('Deploy Image') {
    steps{
        script {
            docker.withRegistry( '', registryCredential ) {
                dockerImage.push()
            }
        }
    }
}
```

O código final será (lembre-se, usando suas environment):

```Jenkinsfile
pipeline {
    environment {
        registry = "gustavoapolinario/docker-test"
        registryCredential = 'dockerhub'
        dockerImage = ''
    }
   
    agent any
    
    stages {
        
        stage('Cloning Git') {
            steps {
                git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'

            }
        }
        
    	stage('Building image') {
            steps{
                script {
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
                }
    	    }
    	}
    	stage('Deploy Image') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential ) {
                        dockerImage.push()
                    }
                }
            }
        }
	}
}
```

Salve e execute-o.

Incrível, o build está completo e a imagem será enviada para o repositório docker.


## Pipeline completo para uma aplicação node.js

Estes passos são para quem fez este tutorial: [Iniciando com Pipeline no Jenkins executando teste de Node.js]({{< ref "post/jenkins-starting-with-pipeline-doing-a-nodejs-test.pt.md" >}}).

Para completar o pipeline do node.js, vamos mudar o pipeline para juntar com o build e teste do node.js.

O pipeline completo irá ser:

```Jenkinsfile
pipeline {
    environment {
        registry = "gustavoapolinario/docker-test"
        registryCredential = 'dockerhub'
        dockerImage = ''
    }
   
    agent any
    
    tools {nodejs "node" }
    
    stages {
        
        stage('Cloning Git') {
            steps {
                git 'https://github.com/gustavoapolinario/microservices-node-example-todo-frontend.git'

            }
        }
        
    	stage('Build') {
            steps {
    		    sh 'npm install'
    		    sh 'npm run bowerInstall'
            }
    	}
    	
    	stage('Test') {
            steps {
    		    sh 'npm test'
            }
    	}
    	
    	stage('Building image') {
            steps{
                script {
                    dockerImage = docker.build registry + ":$BUILD_NUMBER"
                }
    	    }
    	}

    	stage('Deploy Image') {
            steps{
                script {
                    docker.withRegistry( '', registryCredential ) {
                        dockerImage.push()
                    }
                }
            }
        }
	}
}
```

Agora você tem um pipeline para testar e criar uma imagem docker para sua aplicação.

Obrigado.
