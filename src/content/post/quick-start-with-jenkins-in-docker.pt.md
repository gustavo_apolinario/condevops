---
author: "Gustavo Apolinario"
date: 2018-03-08
title: "Iniciando com jenkins no docker"
slug: "iniciando-com-jenkins-no-docker"
tags: [
    "jenkins",
    "docker",
    "pipeline",
    "blue ocean",
    "devops",
]
categories: [
    "jenkins"
]
---



## Introdução

O que é Jenkins? Jenkins é uma ferramente de automação. Com ele, você pode criar jobs para fazer build, test e deploy da sua aplicação.

{{< figure src="/jenkins/jenkins-docker-pipeline.jpg" alt="Jenkins, docker e Pipeline" class="center" >}}

Você pode criar uma pipeline no jenkins. Mas o que é pipeline? Pipeline é um script passo a passo de tudo que você precisa fazer. Exemplo: você irá criar uma pipeline para dar build do seu código. O primeiro passo é dar o checkout do seu código git, depois disso você precisa realizar os testes, dar o build, empacotar, criar a imagem do docker e, finalmente, fazer deploy do seu código.

<!--more-->

## Criando Container

Para o primeiro passo com jenkins, vamos criar um container na sua máquina.
Você precisa do docker instalado e rodando em sua máquina.
Execute este comando:

```sh
docker run -—name jenkinsci -p 8080:8080 jenkins/jenkins:lts
```

Este comando irá fazer download e executar um jenkins na sua máquina.

Aguarde um tempo, o console irá mostrar várias mensagens. Quando o jenkins estiver inicializado, o console irá mostra-lo a senha de admin para iniciar o setup inicial. O texto com a senha é:

```sh
*************************************************************
*************************************************************
*************************************************************
Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:
a3c76c0cc3e541ff94b3670c22995342
This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
*************************************************************
*************************************************************
*************************************************************
```

## Assistente de configuração inicial

Você pode acessar seu jenkins em http://localhost:8080/. Você irá ver esta tela:

{{< figure src="/jenkins/installing/jenkins-installing-unlock.png" alt="Tela de desbloqueio" class="center" >}}

Coloque a senha de admin no input da tela e clique em "continue".

Na próxima tela, clique em “Install suggested plugins”.

{{< figure src="/jenkins/installing/installing-customize.png" alt="Tela de customização" class="center" >}}

Aguarde a instalação dos plugins.

{{< figure src="/jenkins/installing/installing-customize-2.png" alt="Tela de inicialização. Fazendo download dos plugins" class="center" >}}

Agora crie um usuário para você.

{{< figure src="/jenkins/installing/installing-create-user.png" alt="Tela de Criação de usuário" class="center" >}}

Terminamos a instalação, clique em “Get start using Jenkins”.

{{< figure src="/jenkins/installing/installing-finished.png" alt="Jenkins está pronto" class="center" >}}

## Configuração inicial

Vamos instalar 2 plugins:

- blueocean plugin para embelezar o jenkins. Este plugin melhora a usabilidade do jenkins e foca em scripts com pipeline.
- locale plugin para sempre mostrar o jenkins em inglês. Você não precisa deste plugin, mas eu recomendo a instalação para procurar os erros diretamente em inglês e ter mais chance de encontrar a solução.

### Instalando o plugin locale

Vá para o Plugn Manager e busque por locale.

Para instalar plugins:

Na home do jenkins, clique em **Manage Jenkins**:

{{< figure src="/jenkins/jenkins-menu-without-blueocean.png" alt="menu do jenkins" class="center" >}}

Clique em **Manage Plugins**:

{{< figure src="/jenkins/manage/plugins/manage-plugins.png" alt="Link do Manage Plugins" class="center" >}}

Em Plugin manager, você precisa clicar na aba **Available** e depois que carregar a aba, procurar o plugin.

{{< figure src="/jenkins/manage/plugins/all-tabs.png" alt="Abas para buscar e gerenciar os plugins" class="center" >}}

Busque por **locale**, marque o checkbox e click no botão install.

{{< figure src="/jenkins/manage/plugins/check-locale.png" alt="locale plugin" class="center" >}}

### Configurando locale

Depois de instalar o locale, vá em Home > Manage Jenkins > Configure System.

{{< figure src="/jenkins/manage/configure-jenkins/configure-jenkins.png" alt="Configure Jenkins" class="center" >}}

Busque por locale. Coloque **en** no campo **“Default language”** e marque o checkbox **“ignore browser preference and force this language to all users”**.

{{< figure src="/jenkins/manage/configure-jenkins/configure-jenkins-locale.png" alt="Locale configurado" class="center" >}}

### Instalando o plugin blue ocean

Com o plugin Locale instalado e configurado, vamos instalar o Blue ocean.
Volte para a home do jenkins > Manage jenkins >Manage Plugins. Na aba Available busque por blueocean e instale-o.

{{< figure src="/jenkins/manage/plugins/check-blue-ocean.png" alt="blueocean plugin" class="center" >}}

### Trocando o visual para blue ocean

Tudo certo mas,  o visual está sem o plugin blue ocean. Vamos passar a usá-lo.
Na home do jenkins, clique em **“Open Blue Ocean”** (menu na esquerda).

{{< figure src="/jenkins/jenkins-menu.png" alt="menu do jenkins com blue ocean" class="center" >}}

Agora, você verá uma tela como essa:

{{< figure src="/jenkins/jenkins-blueocean-pipeline.png" alt="Blue ocean" class="center" >}}

Tudo certo. O jenkins está instalado e o plugin blue ocean está sendo usado para ter uma melhor experiência com nossos pipelines.

O próximo passo será criar uma pipeline para darmos build.
